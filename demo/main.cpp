#include "rk_backend.h"
#include <iostream>

int main() {
    // 模型路径和设备配置
    std::string model_path = "resnet18_for_rk3588.rknn";
    std::string device = "npu";

    // 创建RKNN_Backend实例
    RKNN_Backend backend(model_path, device);

    // 假设模型输入为std::any类型，这里使用一个简单的示例
    // std::any model_input = /* Your model input */[1];
    std::unordered_map<std::string, std::any> kwargs;

    // 调用模型的forward函数
    // std::any result = backend(model_input, kwargs);

    // 处理输出结果
    // 根据模型的输出类型进行转换和处理
    // std::cout << std::any_cast<ExpectedType>(result) << std::endl;

    return 0;
}

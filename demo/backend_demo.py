import sys
import os

# 将 lib.linux-x86_64-3.8 目录添加到 PYTHONPATH 中
sys.path.append(
    "/workspace/gear-train-py/geartrain/backends/backends_cpp/rknn/build/lib.linux-x86_64-3.8"
)

import rk_backend_cpp

model_path = "resnet18_for_rk3588.rknn"
backend = rk_backend_cpp.RKNN_Backend(model_path, "npu")

# 如何交叉编译C++并构建成python库

## 1. 交叉编译python
- python版本要和目标平台一致, 本文以python3.10为例
  
```bash
python3 --version
# Python 3.10.12
```
下载python安装包
```bash
wget https://www.python.org/ftp/python/3.10.0/Python-3.10.0.tgz
ls -lh Python-3.10.0.tgz # 查看完整性
tar -xzf Python-3.10.0.tgz
```
- 编译

```bash
mkdir -p /workspace/python3.10-aarch64
cd /workspace/python3.10-aarch64

echo "ac_cv_file__dev_ptmx=yes" > /workspace/config.site
echo "ac_cv_file__dev_ptc=yes" >> /workspace/config.site
export CONFIG_SITE=/workspace/config.site

/workspace/Python-3.10.0/configure --host=aarch64-linux-gnu --build=x86_64-linux-gnu --prefix=/workspace/python3.10-aarch64/install --enable-shared --enable-ipv6 --disable-static --with-ensurepip=yes

make
make install
```

## 2. 交叉编译pybind11

编写交叉编译工具链

`aarch64-toolchain.cmake`
```cmake
# aarch64-toolchain.cmake
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR aarch64)

# 设置交叉编译器
set(CMAKE_C_COMPILER aarch64-linux-gnu-gcc)
set(CMAKE_CXX_COMPILER aarch64-linux-gnu-g++)

# 设置路径（根据实际安装路径调整）
# set(CMAKE_FIND_ROOT_PATH /usr/aarch64-linux-gnu)
set(CMAKE_FIND_ROOT_PATH /workspace/python3.10-aarch64/install)

# 只在目标平台上查找库和头文件
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

```


```bash
git clone https://github.com/pybind/pybind11.git
cd pybind11/
mkdir build
cd build
cmake -DCMAKE_TOOLCHAIN_FILE=aarch64-toolchain.cmake -DCMAKE_INSTALL_PREFIX=/workspace/python3.10-aarch64/install ..

make
make install
```

## 3. 交叉编译demo
### demo 组成
```bash
.
├── CMakeLists.txt
├── aarch64-toolchain.cmake
├── bindings
│   └── python_bindings.cpp
├── build
├── include
│   └── my_math_lib
│       └── math_operations.h
├── src
│   └── math_operations.cpp
└── test.py
```

`math_operations.h`
```cpp
// include/my_math_lib/math_operations.h

#ifndef MY_MATH_LIB_MATH_OPERATIONS_H
#define MY_MATH_LIB_MATH_OPERATIONS_H

namespace my_math_lib {

class MathOperations {
public:
    int add(int a, int b);
    int subtract(int a, int b);
    int multiply(int a, int b);
    double divide(int a, int b);
};

} // namespace my_math_lib

#endif // MY_MATH_LIB_MATH_OPERATIONS_H
```
`math_operations.cpp`
```cpp
// src/math_operations.cpp

#include "my_math_lib/math_operations.h"
#include <stdexcept>

namespace my_math_lib {

int MathOperations::add(int a, int b) {
    return a + b;
}

int MathOperations::subtract(int a, int b) {
    return a - b;
}

int MathOperations::multiply(int a, int b) {
    return a * b;
}

double MathOperations::divide(int a, int b) {
    if (b == 0) {
        throw std::invalid_argument("Division by zero");
    }
    return static_cast<double>(a) / b;
}

} // namespace my_math_lib

```
`python_bindings.cpp`
```cpp
// bindings/python_bindings.cpp

#include <pybind11/pybind11.h>
#include "my_math_lib/math_operations.h"

namespace py = pybind11;

PYBIND11_MODULE(my_math_lib, m) {
    py::class_<my_math_lib::MathOperations>(m, "MathOperations")
        .def(py::init<>())
        .def("add", &my_math_lib::MathOperations::add)
        .def("subtract", &my_math_lib::MathOperations::subtract)
        .def("multiply", &my_math_lib::MathOperations::multiply)
        .def("divide", &my_math_lib::MathOperations::divide);
}

```
`CMakeLists.txt`
```cmake
cmake_minimum_required(VERSION 3.12)
project(MyMathLib)

# 设置C++标准
set(CMAKE_CXX_STANDARD 14)

# 设置Python路径
set(Python3_ROOT_DIR /workspace/python3.10-aarch64/install)
set(Python3_INCLUDE_DIR /workspace/python3.10-aarch64/install/include/python3.10)
set(Python3_LIBRARY /workspace/python3.10-aarch64/install/lib/libpython3.10.so)

# 查找Python3
find_package(Python3 3.10 REQUIRED COMPONENTS Interpreter Development)

# 查找pybind11
find_package(pybind11 REQUIRED)

# 包含头文件
include_directories(${Python3_INCLUDE_DIRS} /workspace/example/libtest/include)

# 添加pybind11模块
pybind11_add_module(my_math_lib MODULE bindings/python_bindings.cpp src/math_operations.cpp)

# 链接Python库
target_link_libraries(my_math_lib PRIVATE ${Python3_LIBRARIES})

```

### 编译

```bash
cmake -DCMAKE_TOOLCHAIN_FILE=../aarch64-toolchain.cmake -DPYTHON_EXECUTABLE=$(which python3.10) ..

make
```

## 4. python 调用
`test.py`
```python
import my_math_lib
def main():
    math_ops = my_math_lib.MathOperations()
    print(f"Add: 3 + 4 = {math_ops.add(3, 4)}")
    print(f"Subtract: 10 - 6 = {math_ops.subtract(10, 6)}")
    print(f"Multiply: 2 * 5 = {math_ops.multiply(2, 5)}")
    try:
        print(f"Divide: 8 / 2 = {math_ops.divide(8, 2)}")
        print(f"Divide: 8 / 0 = {math_ops.divide(8, 0)}")
    except ValueError as e:
        print(f"Error: {e}")


if __name__ == "__main__":
    main()

```

```bash
cp my_math_lib.cpython-310-aarch64-linux-gnu.so path/to/lib/
export PYTHONPATH=$PYTHONPATH:path/to/lib
python test.py
```
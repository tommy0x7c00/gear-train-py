
# [`class Pipeline`](../geartrain/pipelines/base.py)
`Pipeline` 的输入输出均为 `dict`

## `Pipeline` 初始化

## `Pipeline` 的子类必须实现下列函数:

### `preprocess`
```python
def preprocess(
    self, input_: Any, **preprocess_parameters: Dict
) -> Dict[str, GenericTensor]:
```
- `args`:

    - `input_`: 首选字典类型 (便于组合`Pipeline`), 也可以是其他
    - `preprocess_parameters`: 预处理器的参数, 来自函数 `_sanitize_parameters`
- `return`: `Dict`
    - `model_inputs`: `self.model` 推理的输入数据
    - `others`: 其他参数
            

### `_forward`
```python
def _forward(
    self, input_tensors: Dict[str, GenericTensor], **forward_parameters: Dict
) -> ModelOutput:
```
- `args`:

    - `input_tensors`: 字典类型, 即 `preprocess` 的 `return` 数据
    - `forward_parameters`: 模型推理的参数, 来自函数 `_sanitize_parameters`
- `return`: `Dict`
    - `model_outputs`: `self.model` 推理的输出数据
    - `others`: 其他参数
### `postprocess`
```python
def postprocess(
    self, model_outputs: ModelOutput, **postprocess_parameters: Dict
) -> Any:
```
- `args`:

    - `model_outputs`: 字典类型, 即 `_forward` 的 `return` 数据
    - `postprocess_parameters`: 后处理的参数, 来自函数 `_sanitize_parameters`
- `return`: `Dict`
    - `model_outputs`: 后处理输出数据
    - `others`: 其他参数

### `_sanitize_parameters`: 

参数处理, 在管道初始化和管道被调用时调用

```python
def _sanitize_parameters(self, **pipeline_parameters):
```

- `args`:
    - `pipeline_parameters`: 字典类型, 即 `pipeline.__call__` 的`kwds`数据
- `return`:
    - `preprocess_parameters`: `Dict` 预处理器的参数, 来自函数 `_sanitize_parameters`

    - `forward_parameters`: `Dict` 模型推理的参数, 来自函数 `_sanitize_parameters`

    - `postprocess_parameters`: `Dict` 后处理的参数, 来自函数 `_sanitize_parameters`







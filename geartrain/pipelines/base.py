# coding=utf-8
# Copyright 2018 The HuggingFace Inc. team.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import csv
import json
import os
import pickle
import sys
import types
from abc import ABC, abstractmethod
from collections import UserDict
from contextlib import contextmanager
from os.path import abspath, exists
from typing import TYPE_CHECKING, Any, Dict, List, Optional, Tuple, Union
import traceback
import numpy as np
from geartrain.utils.utils import Profile

# from ..feature_extraction_utils import PreTrainedFeatureExtractor
from ..modelcard import ModelCard
from ..tokenization_utils import PreTrainedTokenizer
from ..utils import (
    ModelOutput,
    is_tf_available,
    is_torch_available,
    is_torch_cuda_available,
    is_torch_mlu_available,
    is_torch_mps_available,
    is_torch_npu_available,
    is_torch_xpu_available,
    logging,
)

from ..backends.base import Backend

GenericTensor = Union[List["GenericTensor"], "torch.Tensor", "tf.Tensor"]

if is_tf_available():
    import tensorflow as tf


if is_torch_available():
    import torch
    from torch.utils.data import DataLoader, Dataset

    # Re-export for backward compatibility
else:
    Dataset = None
    KeyDataset = None

logger = logging.get_logger(__name__)
# 🚀✅⚠️❌


class PipelineException(Exception):
    """
    Raised by a [`Pipeline`] when handling __call__.

    Args:
        task (`str`): The task of the pipeline.
        model (`str`): The model used by the pipeline.
        reason (`str`): The error message to display.
    """

    def __init__(self, task: str, model: str, reason: str):
        super().__init__(reason)

        self.task = task
        self.model = model


class ArgumentHandler(ABC):
    """
    Base interface for handling arguments for each [`~pipelines.Pipeline`].
    """

    @abstractmethod
    def __call__(self, *args, **kwargs):
        raise NotImplementedError()


class PipelineDataFormat:
    """
    Base class for all the pipeline supported data format both for reading and writing. Supported data formats
    currently includes:

    - JSON
    - CSV
    - stdin/stdout (pipe)

    `PipelineDataFormat` also includes some utilities to work with multi-columns like mapping from datasets columns to
    pipelines keyword arguments through the `dataset_kwarg_1=dataset_column_1` format.

    Args:
        output_path (`str`): Where to save the outgoing data.
        input_path (`str`): Where to look for the input data.
        column (`str`): The column to read.
        overwrite (`bool`, *optional*, defaults to `False`):
            Whether or not to overwrite the `output_path`.
    """

    SUPPORTED_FORMATS = ["json", "csv", "pipe"]

    def __init__(
        self,
        output_path: Optional[str],
        input_path: Optional[str],
        column: Optional[str],
        overwrite: bool = False,
    ):
        self.output_path = output_path
        self.input_path = input_path
        self.column = column.split(",") if column is not None else [""]
        self.is_multi_columns = len(self.column) > 1

        if self.is_multi_columns:
            self.column = [
                tuple(c.split("=")) if "=" in c else (c, c) for c in self.column
            ]

        if output_path is not None and not overwrite:
            if exists(abspath(self.output_path)):
                raise OSError(f"{self.output_path} already exists on disk")

        if input_path is not None:
            if not exists(abspath(self.input_path)):
                raise OSError(f"{self.input_path} doesnt exist on disk")

    @abstractmethod
    def __iter__(self):
        raise NotImplementedError()

    @abstractmethod
    def save(self, data: Union[dict, List[dict]]):
        """
        Save the provided data object with the representation for the current [`~pipelines.PipelineDataFormat`].

        Args:
            data (`dict` or list of `dict`): The data to store.
        """
        raise NotImplementedError()

    def save_binary(self, data: Union[dict, List[dict]]) -> str:
        """
        Save the provided data object as a pickle-formatted binary data on the disk.

        Args:
            data (`dict` or list of `dict`): The data to store.

        Returns:
            `str`: Path where the data has been saved.
        """
        path, _ = os.path.splitext(self.output_path)
        binary_path = os.path.extsep.join((path, "pickle"))

        with open(binary_path, "wb+") as f_output:
            pickle.dump(data, f_output)

        return binary_path

    @staticmethod
    def from_str(
        format: str,
        output_path: Optional[str],
        input_path: Optional[str],
        column: Optional[str],
        overwrite=False,
    ) -> "PipelineDataFormat":
        """
        Creates an instance of the right subclass of [`~pipelines.PipelineDataFormat`] depending on `format`.

        Args:
            format (`str`):
                The format of the desired pipeline. Acceptable values are `"json"`, `"csv"` or `"pipe"`.
            output_path (`str`, *optional*):
                Where to save the outgoing data.
            input_path (`str`, *optional*):
                Where to look for the input data.
            column (`str`, *optional*):
                The column to read.
            overwrite (`bool`, *optional*, defaults to `False`):
                Whether or not to overwrite the `output_path`.

        Returns:
            [`~pipelines.PipelineDataFormat`]: The proper data format.
        """
        if format == "json":
            return JsonPipelineDataFormat(
                output_path, input_path, column, overwrite=overwrite
            )
        elif format == "csv":
            return CsvPipelineDataFormat(
                output_path, input_path, column, overwrite=overwrite
            )
        elif format == "pipe":
            return PipedPipelineDataFormat(
                output_path, input_path, column, overwrite=overwrite
            )
        else:
            raise KeyError(
                f"Unknown reader {format} (Available reader are json/csv/pipe)"
            )


class CsvPipelineDataFormat(PipelineDataFormat):
    """
    Support for pipelines using CSV data format.

    Args:
        output_path (`str`): Where to save the outgoing data.
        input_path (`str`): Where to look for the input data.
        column (`str`): The column to read.
        overwrite (`bool`, *optional*, defaults to `False`):
            Whether or not to overwrite the `output_path`.
    """

    def __init__(
        self,
        output_path: Optional[str],
        input_path: Optional[str],
        column: Optional[str],
        overwrite=False,
    ):
        super().__init__(output_path, input_path, column, overwrite=overwrite)

    def __iter__(self):
        with open(self.input_path, "r") as f:
            reader = csv.DictReader(f)
            for row in reader:
                if self.is_multi_columns:
                    yield {k: row[c] for k, c in self.column}
                else:
                    yield row[self.column[0]]

    def save(self, data: List[dict]):
        """
        Save the provided data object with the representation for the current [`~pipelines.PipelineDataFormat`].

        Args:
            data (`List[dict]`): The data to store.
        """
        with open(self.output_path, "w") as f:
            if len(data) > 0:
                writer = csv.DictWriter(f, list(data[0].keys()))
                writer.writeheader()
                writer.writerows(data)


class JsonPipelineDataFormat(PipelineDataFormat):
    """
    Support for pipelines using JSON file format.

    Args:
        output_path (`str`): Where to save the outgoing data.
        input_path (`str`): Where to look for the input data.
        column (`str`): The column to read.
        overwrite (`bool`, *optional*, defaults to `False`):
            Whether or not to overwrite the `output_path`.
    """

    def __init__(
        self,
        output_path: Optional[str],
        input_path: Optional[str],
        column: Optional[str],
        overwrite=False,
    ):
        super().__init__(output_path, input_path, column, overwrite=overwrite)

        with open(input_path, "r") as f:
            self._entries = json.load(f)

    def __iter__(self):
        for entry in self._entries:
            if self.is_multi_columns:
                yield {k: entry[c] for k, c in self.column}
            else:
                yield entry[self.column[0]]

    def save(self, data: dict):
        """
        Save the provided data object in a json file.

        Args:
            data (`dict`): The data to store.
        """
        with open(self.output_path, "w") as f:
            json.dump(data, f)


class PipedPipelineDataFormat(PipelineDataFormat):
    """
    Read data from piped input to the python process. For multi columns data, columns should separated by \t

    If columns are provided, then the output will be a dictionary with {column_x: value_x}

    Args:
        output_path (`str`): Where to save the outgoing data.
        input_path (`str`): Where to look for the input data.
        column (`str`): The column to read.
        overwrite (`bool`, *optional*, defaults to `False`):
            Whether or not to overwrite the `output_path`.
    """

    def __iter__(self):
        for line in sys.stdin:
            # Split for multi-columns
            if "\t" in line:
                line = line.split("\t")
                if self.column:
                    # Dictionary to map arguments
                    yield {kwargs: l for (kwargs, _), l in zip(self.column, line)}
                else:
                    yield tuple(line)

            # No dictionary to map arguments
            else:
                yield line

    def save(self, data: dict):
        """
        Print the data.

        Args:
            data (`dict`): The data to store.
        """
        print(data)

    def save_binary(self, data: Union[dict, List[dict]]) -> str:
        if self.output_path is None:
            raise KeyError(
                "When using piped input on pipeline outputting large object requires an output file path. "
                "Please provide such output path through --output argument."
            )

        return super().save_binary(data)


class _ScikitCompat(ABC):
    """
    Interface layer for the Scikit and Keras compatibility.
    """

    @abstractmethod
    def transform(self, X):
        raise NotImplementedError()

    @abstractmethod
    def predict(self, X):
        raise NotImplementedError()


class Pipeline(_ScikitCompat):
    """
    The Pipeline class is the class from which all pipelines inherit. Refer to this class for methods shared across
    different pipelines.

    Base class implementing pipelined operations. Pipeline workflow is defined as a sequence of the following
    operations:

        Input -> Tokenization -> Model Inference -> Post-Processing (task dependent) -> Output

    Pipeline supports running on CPU or GPU through the device argument (see below).

    Some pipeline, like for instance [`FeatureExtractionPipeline`] (`'feature-extraction'`) output large tensor object
    as nested-lists. In order to avoid dumping such large structure as textual data we provide the `binary_output`
    constructor argument. If set to `True`, the output will be stored in the pickle format.
    """

    default_input_names = None

    def __init__(
        self,
        model: Optional[Union[Backend, "Pipeline"]] = None,
        tokenizer: Optional[PreTrainedTokenizer] = None,
        # feature_extractor: Optional[PreTrainedFeatureExtractor] = None,
        # image_processor: Optional[BaseImageProcessor] = None,
        modelcard: Optional[ModelCard] = None,
        framework: Optional[str] = None,
        task: str = "",
        args_parser: ArgumentHandler = None,
        device: Union[int, "torch.device"] = None,
        torch_dtype: Optional[Union[str, "torch.dtype"]] = None,
        binary_output: bool = False,
        profile: bool = False,
        verbose: bool = False,
        **kwargs,
    ):

        if model is not None:
            if framework is None:
                # framework, model = infer_framework_load_model(model, config=model.config)
                framework = model.framework

        feature_extractor = None
        self.profile = profile
        self.task = task
        self.speed=None
        self.model = model
        self.tokenizer = tokenizer
        self.feature_extractor = feature_extractor
        if "image_processor" in kwargs.keys():
            self.image_processor = kwargs["image_processor"]
        self.modelcard = modelcard
        self.framework = framework
        self.verbose = verbose

        # `accelerate` device map
        hf_device_map = getattr(self.model, "hf_device_map", None)

        if hf_device_map is not None and device is not None:
            raise ValueError(
                "The model has been loaded with `accelerate` and therefore cannot be moved to a specific device. Please "
                "discard the `device` argument when creating your pipeline object."
            )

        if device is None:
            if hf_device_map is not None:
                # Take the first device used by `accelerate`.
                device = next(iter(hf_device_map.values()))
            else:
                device = -1

        if is_torch_available() and self.framework == "pt":
            if device == -1 and self.model.device is not None:
                device = self.model.device
            if isinstance(device, torch.device):
                if device.type == "xpu" and not is_torch_xpu_available(
                    check_device=True
                ):
                    raise ValueError(
                        f'{device} is not available, you should use device="cpu" instead'
                    )
                self.device = device
            elif isinstance(device, str):
                if "xpu" in device and not is_torch_xpu_available(check_device=True):
                    raise ValueError(
                        f'{device} is not available, you should use device="cpu" instead'
                    )
                self.device = torch.device(device)
            elif device < 0:
                self.device = torch.device("cpu")
            elif is_torch_mlu_available():
                self.device = torch.device(f"mlu:{device}")
            elif is_torch_cuda_available():
                self.device = torch.device(f"cuda:{device}")
            elif is_torch_npu_available():
                self.device = torch.device(f"npu:{device}")
            elif is_torch_xpu_available(check_device=True):
                self.device = torch.device(f"xpu:{device}")
            elif is_torch_mps_available():
                self.device = torch.device(f"mps:{device}")
            else:
                raise ValueError(f"{device} unrecognized or not available.")
        else:
            self.device = device if device is not None else -1

        self.binary_output = binary_output
        # We shouldn't call `model.to()` for models loaded with accelerate as well as the case that model is already on device
        if (
            self.framework == "pt"
            and self.model.device != self.device
            and not (isinstance(self.device, int) and self.device < 0)
            and hf_device_map is None
        ):
            self.model.to(self.device)

        self.call_count = 0
        self._batch_size = kwargs.pop("batch_size", None)
        self._num_workers = kwargs.pop("num_workers", None)
        self._preprocess_params, self._forward_params, self._postprocess_params = (
            self._sanitize_parameters(**kwargs)
        )
        logger.info("{}: pipeline init successfully ✅".format(self.task))

    def to(self, device=None):
        if self.model is not None:
            self.model.to(device)

    def transform(self, X):
        """
        Scikit / Keras interface to transformers' pipelines. This method will forward to __call__().
        """
        return self(X)

    def predict(self, X):
        """
        Scikit / Keras interface to transformers' pipelines. This method will forward to __call__().
        """
        return self(X)

    @property
    def torch_dtype(self) -> Optional["torch.dtype"]:
        """
        Torch dtype of the model (if it's Pytorch model), `None` otherwise.
        """
        return getattr(self.model, "dtype", None)

    @contextmanager
    def device_placement(self):
        """
        Context Manager allowing tensor allocation on the user-specified device in framework agnostic way.

        Returns:
            Context manager

        Examples:

        ```python
        # Explicitly ask for tensor allocation on CUDA device :0
        pipe = pipeline(..., device=0)
        with pipe.device_placement():
            # Every framework specific tensor allocation will be done on the request device
            output = pipe(...)
        ```"""
        if self.framework == "tf":
            with tf.device(
                "/CPU:0" if self.device == -1 else f"/device:GPU:{self.device}"
            ):
                yield
        elif self.framework == "pt":
            if self.device.type == "cuda":
                with torch.cuda.device(self.device):
                    yield
            elif self.device.type == "mlu":
                with torch.mlu.device(self.device):
                    yield
            else:
                yield
        else:
            yield

    def ensure_tensor_on_device(self, **inputs):
        """
        Ensure PyTorch tensors are on the specified device.

        Args:
            inputs (keyword arguments that should be `torch.Tensor`, the rest is ignored):
                The tensors to place on `self.device`.
            Recursive on lists **only**.

        Return:
            `Dict[str, torch.Tensor]`: The same as `inputs` but on the proper device.
        """
        return self._ensure_tensor_on_device(inputs, self.device)

    def _ensure_tensor_on_device(self, inputs, device):
        if isinstance(inputs, ModelOutput):
            return ModelOutput(
                {
                    name: self._ensure_tensor_on_device(tensor, device)
                    for name, tensor in inputs.items()
                }
            )
        elif isinstance(inputs, dict):
            return {
                name: self._ensure_tensor_on_device(tensor, device)
                for name, tensor in inputs.items()
            }
        elif isinstance(inputs, UserDict):
            return UserDict(
                {
                    name: self._ensure_tensor_on_device(tensor, device)
                    for name, tensor in inputs.items()
                }
            )
        elif isinstance(inputs, list):
            return [self._ensure_tensor_on_device(item, device) for item in inputs]
        elif isinstance(inputs, tuple):
            return tuple(
                [self._ensure_tensor_on_device(item, device) for item in inputs]
            )
        elif isinstance(inputs, torch.Tensor):
            return inputs.to(device)
        else:
            return inputs

    @abstractmethod
    def _sanitize_parameters(self, **pipeline_parameters):
        """
        _sanitize_parameters will be called with any excessive named arguments from either `__init__` or `__call__`
        methods. It should return 3 dictionaries of the resolved parameters used by the various `preprocess`,
        `forward` and `postprocess` methods. Do not fill dictionaries if the caller didn't specify a kwargs. This
        lets you keep defaults in function signatures, which is more "natural".

        It is not meant to be called directly, it will be automatically called and the final parameters resolved by
        `__init__` and `__call__`
        """
        raise NotImplementedError("_sanitize_parameters not implemented")

    @abstractmethod
    def preprocess(
        self, input_: Any, **preprocess_parameters: Dict
    ) -> Dict[str, GenericTensor]:
        """
        Preprocess will take the `input_` of a specific pipeline and return a dictionary of everything necessary for
        `_forward` to run properly. It should contain at least one tensor, but might have arbitrary other items.
        """
        raise NotImplementedError("preprocess not implemented")

    @abstractmethod
    def _forward(
        self, input_tensors: Dict[str, GenericTensor], **forward_parameters: Dict
    ) -> ModelOutput:
        """
        模型输入key: "model_inputs"

        _forward will receive the prepared dictionary from `preprocess` and run it on the model. This method might
        involve the GPU or the CPU and should be agnostic to it. Isolating this function is the reason for `preprocess`
        and `postprocess` to exist, so that the hot path, this method generally can run as fast as possible.

        It is not meant to be called directly, `forward` is preferred. It is basically the same but contains additional
        code surrounding `_forward` making sure tensors and models are on the same device, disabling the training part
        of the code (leading to faster inference).
        """
        raise NotImplementedError("_forward not implemented")

    @abstractmethod
    def postprocess(
        self, model_outputs: ModelOutput, **postprocess_parameters: Dict
    ) -> Any:
        """
        Postprocess will receive the raw outputs of the `_forward` method, generally tensors, and reformat them into
        something more friendly. Generally it will output a list or a dict or results (containing just strings and
        numbers).
        """
        raise NotImplementedError("postprocess not implemented")

    def get_inference_context(self):
        return torch.no_grad

    def forward(self, model_inputs, **forward_params):
        with self.device_placement():
            if self.framework == "tf":
                model_outputs = self._forward(model_inputs, **forward_params)
            elif self.framework == "pt":
                inference_context = self.get_inference_context()
                with inference_context():
                    model_inputs = self._ensure_tensor_on_device(
                        model_inputs, device=self.device
                    )
                    model_outputs = self._forward(model_inputs, **forward_params)
                    model_outputs = self._ensure_tensor_on_device(
                        model_outputs, device=torch.device("cpu")
                    )
            elif self.framework in ["ort", "rknn", "x86rknn", "py"]:  # onnxruntime
                model_outputs = self._forward(model_inputs, **forward_params)
            else:
                logger.error("format: {}".format(self.framework))
                raise ValueError(f"Framework {self.framework} is not supported")

        return model_outputs

    def __call__(self, inputs, *args, num_workers=None, batch_size=None, **kwargs):
        if args:
            logger.warning(f"Ignoring args : {args}")

        if num_workers is None:
            if self._num_workers is None:
                num_workers = 0
            else:
                num_workers = self._num_workers
        if batch_size is None:
            if self._batch_size is None:
                batch_size = 1
            else:
                batch_size = self._batch_size

        preprocess_params, forward_params, postprocess_params = (
            self._sanitize_parameters(**kwargs)
        )

        # Fuse __init__ params and __call__ params without modifying the __init__ ones.
        preprocess_params = {**self._preprocess_params, **preprocess_params}
        forward_params = {**self._forward_params, **forward_params}
        postprocess_params = {**self._postprocess_params, **postprocess_params}

        self.call_count += 1
        if (
            self.call_count > 10
            and self.framework == "pt"
            and self.device.type == "cuda"
        ):
            logger.warning_once(
                "You seem to be using the pipelines sequentially on GPU. In order to maximize efficiency please use a"
                " dataset",
            )

        is_dataset = Dataset is not None and isinstance(inputs, Dataset)
        is_generator = isinstance(inputs, types.GeneratorType)
        is_list = isinstance(inputs, list)
        is_dict = isinstance(inputs, dict)

        is_iterable = is_dataset or is_generator or is_list
        if "benchmark" in kwargs.keys():
            if kwargs["benchmark"]:
                return self.benchmark(
                    inputs,
                    preprocess_params,
                    forward_params,
                    postprocess_params,
                    kwargs["num_iterations"],
                )
        pipe_res = self.run_single(
            inputs, preprocess_params, forward_params, postprocess_params
        )
        if self.verbose:
            logger.info(self.task+": " + pipe_res[0].verbose() + "{:.2f}ms".format(sum(self.speed.values())))
        
        return pipe_res

    def run_multi(self, inputs, preprocess_params, forward_params, postprocess_params):
        return [
            self.run_single(item, preprocess_params, forward_params, postprocess_params)
            for item in inputs
        ]

    def run_single(self, inputs, preprocess_params, forward_params, postprocess_params):
        profilers = (
            Profile(device=self.device),
            Profile(device=self.device),
            Profile(device=self.device),
        )
        try:
            with profilers[0]:
                model_inputs = self.preprocess(inputs, **preprocess_params)
        except AttributeError as e:
            logger.error("preprocess error: ")
            traceback.print_exc()
        try:
            with profilers[1]:
                model_outputs = self.forward(model_inputs, **forward_params)
        except AttributeError as e:
            logger.error(f"forward error: ")
            traceback.print_exc()

        try:
            with profilers[2]:
                outputs = self.postprocess(model_outputs, **postprocess_params)
        except AttributeError as e:
            logger.error(f"postprocess error:")
            traceback.print_exc()
        speed_str = "Speed {:.2f}ms preproces {:.2f}ms inference, {:.2f}ms postprocess".format(
            profilers[0].dt * 1e3,
            profilers[1].dt * 1e3,
            profilers[2].dt * 1e3
        )
        self.speed = {
            "preproces": profilers[0].dt * 1e3,
            "inference": profilers[1].dt * 1e3,
            "postprocess": profilers[2].dt * 1e3
        }
        logger.debug(speed_str)
        
        return outputs

    def benchmark(
        self,
        inputs,
        preprocess_params,
        forward_params,
        postprocess_params,
        num_iterations,
    ):
        profilers = (
                Profile(device=self.device),
                Profile(device=self.device),
                Profile(device=self.device),
        )

        self.run_single(inputs, preprocess_params, forward_params, postprocess_params)

        with profilers[0]:
        
            for _ in range(num_iterations):
                model_inputs = self.preprocess(inputs, **preprocess_params)

        with profilers[1]:
            for _ in range(num_iterations):
                model_outputs = self.forward( model_inputs.copy() if isinstance(model_inputs, np.ndarray) else model_inputs.clone(), **forward_params)

        with profilers[2]:
        
            for _ in range(num_iterations):
                outputs = self.postprocess(model_outputs.copy() if isinstance(model_outputs, np.ndarray) else model_outputs.clone(), **postprocess_params)
                
        speed = {
            "preprocess": profilers[0].dt * 1e3 / num_iterations,
            "inference": profilers[1].dt * 1e3 / num_iterations,
            "postprocess": profilers[2].dt * 1e3 / num_iterations,  
        }
        
        logger.info(speed)
        return outputs

    def iterate(self, inputs, preprocess_params, forward_params, postprocess_params):
        # This function should become `get_iterator` again, this is a temporary
        # easy solution.
        for input_ in inputs:
            yield self.run_single(
                input_, preprocess_params, forward_params, postprocess_params
            )


class ChunkPipeline(Pipeline):
    def run_single(self, inputs, preprocess_params, forward_params, postprocess_params):
        all_outputs = []
        for model_inputs in self.preprocess(inputs, **preprocess_params):
            model_outputs = self.forward(model_inputs, **forward_params)
            all_outputs.append(model_outputs)
        outputs = self.postprocess(all_outputs, **postprocess_params)
        return outputs


class AttrDictBase:
    def __getattr__(self, item):
        try:
            return self[item]
        except KeyError:
            raise AttributeError(
                f"'{self.__class__.__name__}' object has no attribute '{item}'"
            )

    def __setattr__(self, key, value):
        self[key] = value

    def __delattr__(self, item):
        try:
            del self[item]
        except KeyError:
            raise AttributeError(
                f"'{self.__class__.__name__}' object has no attribute '{item}'"
            )

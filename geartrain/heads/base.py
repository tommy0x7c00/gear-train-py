from abc import ABC, abstractmethod
from typing import Any
from typing import Optional, Union

class BaseHead(ABC):
    @abstractmethod
    def _sanitize_parameters(self, **kwargs):
        """
        _sanitize_parameters will be called with any excessive named arguments from either `__init__` or `__call__`
        methods. It should return 3 dictionaries of the resolved parameters used by the various `preprocess`,
        `forward` and `postprocess` methods. Do not fill dictionaries if the caller didn't specify a kwargs. This
        lets you keep defaults in function signatures, which is more "natural".

        It is not meant to be called directly, it will be automatically called and the final parameters resolved by
        `__init__` and `__call__`
        """
        raise NotImplementedError("_sanitize_parameters not implemented")

    @abstractmethod
    def process(self, pipeline_outputs: Union[dict, None] = None):
        """
        默认处理 pipeline_outputs["inputs"]
        """
        raise NotImplementedError("process not implemented")
    
    def __call__(self, pipeline_outputs, *args: Any, **kwargs: Any) -> Any:
        
        process_params = (
            self._sanitize_parameters(**kwargs)
        )
        
        return self.process(pipeline_outputs, **process_params)
    
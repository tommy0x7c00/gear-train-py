import logging.config
import os
import platform
import sys
from pathlib import Path
from .utils import logging

logging.set_verbosity_info()
logging.enable_explicit_format()
logger = logging.get_logger(__name__)


MACOS, LINUX, WINDOWS = (
    platform.system() == x for x in ["Darwin", "Linux", "Windows"]
)  # environment booleans


# PyTorch Multi-GPU DDP Constants
RANK = int(os.getenv("RANK", -1))
LOCAL_RANK = int(
    os.getenv("LOCAL_RANK", -1)
)  # https://pytorch.org/docs/stable/elastic/run.html

# Other Constants
ARGV = sys.argv or ["", ""]  # sometimes sys.argv = []
FILE = Path(__file__).resolve()
NUM_THREADS = min(
    8, max(1, os.cpu_count() - 1)
)  # number of YOLO multiprocessing threads
ARM64 = platform.machine() in {"arm64", "aarch64"}  # ARM64 booleans

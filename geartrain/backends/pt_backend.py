import torch
from torch import nn
from .base import Backend


class PT_Backend(Backend, nn.Module):
    def __init__(self, device = None,**kwargs) -> None:
        device = torch.device("cpu")
        super().__init__(device, "pt")
        
    
    
from typing import Any

class Backend:
    """"""
    def __init__(self, device, framework, **kwargs) -> None:
        self.device = device
        self.framework = framework
        self._load_model()
        
    def __call__(self, model_inputs, *args: Any, **kwds: Any) -> Any:
        return self.forward(model_inputs,  *args, **kwds)
    
    def forward(self, model_inputs, *args: Any, **kwds: Any) -> Any:
        # kwds["model_outputs"] = 
        return self._forward(model_inputs)
    
    def _forward(self, model_inputs):
        raise NotImplementedError("Must implement its own forward method")
    
    
    def _load_model(self) -> None:
        raise NotImplementedError("Must implement its own _load_model method")
    
        